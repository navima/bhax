
#ifndef NODE_H
#define NODE_H

class Node
{
public:
  // Public attributes
  //  

  char data;


  /**
   * @return com::company::Node
   */
  com::company::Node makeLeft()
  {
  }


  /**
   * @return com::company::Node
   */
  com::company::Node makeRight()
  {
  }


  /**
   * @param  outStream
   */
  void print(java::io::PrintStream outStream)
  {
  }


  /**
   * @param  outStream
   */
  void print(java::io::PrintWriter outStream)
  {
  }

private:
  // Private attributes
  //  

  com::company::Node right;
  com::company::Node parent;

};

#endif // NODE_H
