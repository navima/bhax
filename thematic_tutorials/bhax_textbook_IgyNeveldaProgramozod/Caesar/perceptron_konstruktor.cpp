#ifndef QL_HPP
#define QL_HPP


#include <iostream>
#include <cstdarg>
#include <map>
#include <iterator>
#include <cmath>
#include <random>
#include <limits>
#include <fstream>

#include "nlp.hpp"
#include "qlc.h"

#ifndef Q_LOOKUP_TABLE
class Perceptron
{
public:
	//a ... miatt akármennyi további argumentuma lehet a funkciónak
	Perceptron(int nof, ...)
	{
		//a rétegek száma
		n_layers = nof;


		//a rétegek maguk (a neuronokra mutató mutatók)
		units = new double* [n_layers];

		//a rétegek számossága
		n_units = new int[n_layers];



		//valtozo meretu argumentumlista parseolasahoz szukseges valtozo deklaralasa
		va_list vap;

		//a parseolo valtozonk inicializalasa
		va_start(vap, nof);

		for (int i{ 0 }; i < n_layers; ++i)
		{
			n_units[i] = va_arg(vap, int);	//az argumentumlistabol a kovetkezo argumentumunkat parseoljuk

			if (i)		//az elso iteracion kivul mindig lefut
				units[i] = new double[n_units[i]];	//akkora réteget csinálunk amekkora
		}

		//a parseolo valtozonk megfelelo eldobasa
		va_end(vap);

		//a szomszédos rétegben lévő neuronok közti kapcsolatok súlyai
		weights = new double** [n_layers - 1];

#ifndef RND_DEBUG
		std::random_device init;
		std::default_random_engine gen{ init() };
#else
		std::default_random_engine gen;
#endif

		std::uniform_real_distribution<double> dist(-1.0, 1.0);


		//a súlyok randomizálása
		for (int i{ 1 }; i < n_layers; ++i)	//annyiszor fut le ahány layer van - 1 (mert az elso layerben levo neuronok felé nincs osszekottetés)
		{
			weights[i - 1] = new double* [n_units[i]];

			for (int j{ 0 }; j < n_units[i]; ++j)	//annyiszor fut le ahány neuron van az aktuális layerben
			{
				weights[i - 1][j] = new double[n_units[i - 1]];

				for (int k{ 0 }; k < n_units[i - 1]; ++k)	//annyiszor fut le ahány neuron van az előbbi rétegben
				{
					weights[i - 1][j][k] = dist(gen);	//random súly beállítása
				}
			}
		}
	}

	Perceptron(std::fstream & file)
	{
		file >> n_layers;

		units = new double*[n_layers];
		n_units = new int[n_layers];

		for (int i{ 0 }; i < n_layers; ++i)
		{
			file >> n_units[i];

			if (i)
				units[i] = new double[n_units[i]];
		}

		weights = new double**[n_layers - 1];

		for (int i{ 1 }; i < n_layers; ++i)
		{
			weights[i - 1] = new double *[n_units[i]];

			for (int j{ 0 }; j < n_units[i]; ++j)
			{
				weights[i - 1][j] = new double[n_units[i - 1]];

				for (int k{ 0 }; k < n_units[i - 1]; ++k)
				{
					file >> weights[i - 1][j][k];
				}
			}
		}
	}
#endif