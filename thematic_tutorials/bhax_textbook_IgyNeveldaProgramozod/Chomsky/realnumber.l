%{
/* Elso resz: golbal C kod */
#include <stdio.h>		/* printf */
int realnumbers = 0;	/* globalis valtozo: szamlalo */
%}

/* Kulon dolgok a lexernek */
/* Itt a digit-et defiliáljuk mint [0-9] */
digit	[0-9]



/* V Harmadik resz */
/* Az itt lévő dolgok a
pattern	action
alakban vannak, ahol a pattern egy REGEX pattern string, az action pedig egy C programblokk vagy single satement */
%%
{digit}*(\.{digit}+)?	{
		realnumbers++;
		printf("[realnum=%s %f]", yytext, atof(yytext));
	}
%%
/* Harmadik resz: nyers C kod, ami a generalt kodban lesz */


int main()
{
	yylex ();	/* A lexer meghivasa */
	printf("The number of real numbers is %d\n", realnumbers);	/* A szamlalas vegeredmenyenek kiirasa */
	return 0;
}
