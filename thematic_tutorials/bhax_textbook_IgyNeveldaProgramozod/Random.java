package java.util;

import java.io.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.DoubleConsumer;
import java.util.function.IntConsumer;
import java.util.function.LongConsumer;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.StreamSupport;
import sun.misc.Unsafe;

public class Random implements java.io.Serializable {
	private final AtomicLong seed;
	private static final long multiplier = 0x5DEECE66DL;
	private static final long addend = 0xBL;
	private static final long mask = (1L << 48) - 1;
	private static final double DOUBLE_UNIT = 0x1.0p-53; // 1.0 / (1L << 53)

	public Random() {
		this(seedUniquifier() ^ System.nanoTime());
	}

	//nem vagyok eléggé kvalifikált ahhoz hogy leírjam itt mi történik
	private static long seedUniquifier() {
		// L'Ecuyer, "Tables of Linear Congruential Generators of
		// Different Sizes and Good Lattice Structure", 1999
		for (;;) {
			long current = seedUniquifier.get();
			long next = current * 181783497276652981L;
			if (seedUniquifier.compareAndSet(current, next))
				return next;
		}
	}

	private static final AtomicLong seedUniquifier = new AtomicLong(8682522807148012L);

	protected int next(int bits) {
		long oldseed, nextseed;
		AtomicLong seed = this.seed;
		do {
			oldseed = seed.get();
			nextseed = (oldseed * multiplier + addend) & mask;
		} while (!seed.compareAndSet(oldseed, nextseed));
		return (int) (nextseed >>> (48 - bits));
	}

	public double nextDouble() {
		return (((long) (next(26)) << 27) + next(27)) * DOUBLE_UNIT;
	}


	
	private double nextNextGaussian;				//A következő szám
	private boolean haveNextNextGaussian = false;	//Van-e következő szám

	//Itt a synchronized az igazából csak megvéd a multithreadelt alkalmazásoknál a hibáktól. Kényszeríti a metódust hogy a hozzáfért változókhoz úgy férjen hozzá hogy más metódus éppen nem használja.
	synchronized public double nextGaussian() {
		// See Knuth, ACP, Section 3.4.1 Algorithm C.
		if (haveNextNextGaussian) {			// Itt nézzük meg hogy van-e következő (eltárolt) értékünk
			haveNextNextGaussian = false;		// Ha van, akkor azzal térünk vissza és átállítjuk a tárolt számot jelző boolt.
			return nextNextGaussian;
		} else {							// Ha nincs következő, akkor kiszámolunk 2-t, visszatérünk az elsővel, a másodikat pedig eltároljuk, és átállítjuk a tárolást jelző boolt.
			double v1, v2, s;
			do {
				v1 = 2 * nextDouble() - 1; // between -1 and 1
				v2 = 2 * nextDouble() - 1; // between -1 and 1
				s = v1 * v1 + v2 * v2;
			} while (s >= 1 || s == 0);
			double multiplier = StrictMath.sqrt(-2 * StrictMath.log(s) / s);
			nextNextGaussian = v2 * multiplier;
			haveNextNextGaussian = true;
			return v1 * multiplier;
		}
	}

}